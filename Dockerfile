FROM python:3.9.5

ARG version

LABEL maintainer="mmp@uit.no" \
  toolVersion=$version \
  toolName=multifetch \
  url=https://gitlab.com/uit-sfb/multifetch

WORKDIR /app/multifetch

ENV HOME /app/multifetch

RUN chmod a+rwx $HOME

COPY src/requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY /src/*.py ./

ENTRYPOINT [ "python3", "./wrapper.py" ]
