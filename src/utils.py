from Bio import SeqIO
import pandas as pd
pd.options.mode.chained_assignment = None
import re
import pathlib


"""
------------Common utils------------
"""


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


# Get data from tsv file
def getTSVInput(input):
    df = pd.read_csv(input, sep='\t', engine='python')
    df.columns = df.columns.str.strip()
    return df


# Parse data from EMBL
def parseEMBLFiles(filename):
    for record in SeqIO.parse(filename, "embl"):
        return record


def getBioProjectId(lst):
    bioprojectList = [x for x in lst if 'Project' in x]
    if len(bioprojectList) > 0:
        return str(bioprojectList[0].split(':')[1])
    return ''


def getBioSampleId(record):
    dblink = record.dbxrefs
    biosampleList = [x for x in dblink if 'BioSample' in x]
    if len(biosampleList) > 0:
        return str(biosampleList[0].split(':')[1])
    return ''


def formatColumn(c):
    return ",".join(c.split(';')).strip()


"""
------------BioSample processing------------
"""
LATLON_LIST = ['geographic location (latitude)', 'geographic location (longitude)', 'lat lon', 'lat_lon']


def getProjSampleChecklistValue(data):
    if data:
        return data[0].get('text', '')
    return ''


def processAttributes(key, attr):
    if attr:
        data = attr[0]
        unit = data.get('unit', '')
        text = data.get('text', '')
        res = ('biosample:'+key, ' '.join([text, unit]).strip())
        return res
    return ()


def processLatLon(data):
    if data:
        res = ', '.join([v.get('text') for v in data])
        return ('biosample:loc:lat_lon', res)
    return ('biosample:loc:lat_lon', '')


def geLatLonData(chars):
    res = [chars.get(x,[]) for x in LATLON_LIST]
    #flatten list
    f_res = [item for sublist in res for item in sublist]
    latlon = processLatLon(f_res)
    return latlon


def getLatLonXML(tagsAndVals):
    res = ', '.join([tagsAndVals.get(x, '') for x in LATLON_LIST if tagsAndVals.get(x, '')])
    return ('biosample:loc:lat_lon', res)


"""
------------EMBL file processing------------
"""


def getRecords(input):
    df = getTSVInput(input)
    res = df['genbank_accession'].tolist()
    accList = [acc for acc in res if (acc and str(acc) != 'missing')]
    accList = list(set(accList))
    return accList


def getLostRecordList(input, output):
    pathlib.Path(output).parent.mkdir(parents=True, exist_ok=True)
    if input:
        df = pd.DataFrame(filter(lambda id: id != None, input))
        df.to_csv(output, sep='\t')


"""
------------EMBL metadata processing------------
"""


def removeColumn(s):
    return ",".join(s.split(';'))


def getSubmissionDate(journals):
    submission_date = ""
    if journals:
        regex = r"\d{1,2}-\w{1,3}-\d{4}"
        submission_date = re.findall(regex, journals)
        if submission_date:
            submission_date = submission_date[0]
    return submission_date


def getFeatureData(feat_name, features):
    feats_data = [feat.qualifiers for feat in features if feat.type == "source"]
    if feats_data:
        return feats_data[0].get(feat_name,[''])[0]
    return ''


def getCountryData(dataFromFeatures, dataFromJournal):
    if dataFromFeatures:
        return dataFromFeatures.split(':')
    if dataFromJournal:
        countryAndName = [re.sub("^\d+\s|\d+\-\d+|\s\d+\s|\s\d+$", "", s.strip()).strip() for s in dataFromJournal]
        if len(countryAndName) > 1:
            order = [1, 0]
            countryAndName = [countryAndName[i] for i in order]
        return countryAndName


def getCounryAndName(lst):
    if len(lst) > 1:
        loc_country, loc_name, *_ = lst
    else:
        loc_country, loc_name = lst[0], ''
    return loc_country, loc_name


def flattenList(lst):
    return [item for sublist in lst for item in sublist]


def getCoverage(lst):
    regex = r"\d+\.\d+"
    coverage = flattenList([re.findall(regex, s) for s in lst])
    if coverage:
        return coverage[0]
    return ''


def splitNewLineAndReturnData(lst):
    if lst:
        return [s.split('\n')[0] for s in lst][0]
    return ''


def getVersion(lst):
    tools = flattenList([s.split(';') for s in lst])
    v_list = flattenList([ i.split('v.')[-1:] for i in tools])
    v = flattenList([j.split('\n')[:-1] if len(j.split('\n')) > 1 else [j] for j in v_list])
    v = [ver.strip() for ver in v if ver]
    return v


def removeNewLineChars(s):
    return ''.join(s.split('\n'))


def getToolName(lst):
    tools = flattenList([s.split(';') for s in lst])
    t_list = [removeNewLineChars(i.split('v.')[0].strip()) for i in tools]
    return t_list


def processSWData(lst):
    if len(lst) > 1:
        return '|'.join(lst)
    elif lst:
        return lst[0]
    else:
        return ''


def getSubmittingLab(lst):
    if lst and len(lst) > 1:
        return ' '.join(lst[1].strip().split())
    return ''


def getCommentValue(lst):
    if len(lst) > 1:
        return lst[len(lst)-1].strip()
    elif lst:
        return lst[0].strip()
    else:
        return ''


def getCapitalized(l):
    if str(l) != 'nan':
        l = str(l)
        if "http" not in l:
            l = l.capitalize()
    return l


"""
------------Output table processing------------
"""


def checkForSeparator(tup): # elem = (name from column : name to show)
    elem = tup[0].split('|')
    if len(elem) > 1:
        head = tup[1]
        res = list(map(lambda x: (x, head), elem))
        return res
    else:
        return tup


# flatten list if lists of tuples and tuples
def flattenIrregular(l):
    for el in l:
        if isinstance(el, list) and not isinstance(el, (str, bytes)):
            yield from flattenIrregular(el)
        else:
            yield el


# In this method we create list of tuples of (attribute name from source, attribute name as it must be represented in the resulting table) from
def getAttributeOrder(table):
    sfb_table = getTSVInput(table)
    sfb_table = sfb_table.drop(['Internal'], axis=1, errors='ignore')
    sfb_table = sfb_table.dropna(axis=1, how='all')
    sfb_table = sfb_table.set_index('SFB')
    columns = list(sfb_table.columns)
    ordered_list = [ [(sfb_table.at[i, c], i) for i in list(sfb_table.loc[ (sfb_table[c].notnull()) & (sfb_table[c]!=u'') ].index)] for c in columns ]
    attribute_list = [list(flattenIrregular(list(map(checkForSeparator, l)))) for l in ordered_list] #check if some of the attributes have duplicates separated by |
    naming_list = list(map(lambda x: dict(x), attribute_list))#[dict(l) for l in ordered_list]
    naming_dict = dict(zip(columns, naming_list))
    return naming_dict


def lowercaseColumns(df):
    columns = list(df.columns.values)
    new_columns = list(map(lambda x: x.lower(), columns))#[c.lower() for c in columns]
    df.columns = new_columns
    return df


def formatLocation(l):
    loc = str(l)
    if ':' in loc:
        loc = '|'.join([l.strip() for l in loc.split(':')])
    elif '/' in loc:
        loc = '|'.join([l.strip() for l in loc.split('/')])
    return loc


def getIsolationCountry(lst):
    return [str(l).split('|',1)[0] for l in lst if l]


def getGAZgeoLoc(lst):
    return [str(l).split('|',1)[len(str(l).split('|',1))-1] for l in lst if l]


def getAllAttributes(table):
    all_attrs = getTSVInput(table)['SFB']
    return list(all_attrs)


def getDefaultValues(table):
    vals = getTSVInput(table)['Default']
    return list(vals)

def updateAttributes(df, attrs):
    if not df.empty and attrs:
        as_list = list(df.columns.values)
        new_column_list = [attrs.get(i, '') if attrs.get(i, '') else i for i in as_list]
        df.columns = new_column_list
    return df


def mergeDataFrames(df1, df2):
    if not (df1.empty or df2.empty):
        df_merged = pd.merge(df1, df2, on=list(df1.columns.values)[0], how='left')
        return df_merged
    elif df1.empty:
        return df2
    else:
        return df1


def writeToTSV(df, outPath):
    if not df.empty:
        df.to_csv(outPath, sep='\t')


def sjoin(x):
    return '++'.join(x[x.notnull()].astype(str))
#remove duplicated column values
def removeUnique(l):
    if l[len(l)-1] in ('x', 'y', 'z'):
        l = l[:-1]
    return l


def renameColumns(df):
    columns = list(df.columns.values)
    new_columns = ["_".join(removeUnique(c.split("_"))) for c in columns]
    df.columns = new_columns
    return df


def removeBadValuesAndDuplicates(l):
    if '++' not in str(l):
        return l.strip()
    else:
        vals = [i for i in l.split('++') if i and i != "nan" and not l.isspace()]
        res = "|".join(set(vals))
        return res.strip()


def removeDuplicatedData(df):
    columns = list(df.columns.values)
    for col in columns:
        df[col] = df[col].apply(removeBadValuesAndDuplicates)
    return df


def reorderColumns(df, columns):
    for i, v in enumerate(columns):
        col = df[v]
        df = df.drop(labels=[v], axis=1, errors='ignore')
        df = df.insert(i, v, col)
    return df


def addPrefix(df, columns):
    all_cols = list(df.columns.values)
    additional_info = ['?'+str(c) for c in list(filter(lambda x: x not in columns, all_cols))]
    # additional_info = [i for i in all_cols + columns if i not in all_cols or i not in columns]
    df.columns = columns + additional_info
    return df


def removeVersionNumber(df, column_name):
    if not df.empty:
        df[column_name] = df[column_name].apply(lambda x: x.split('.')[0])
    return df


def checkIfCapitalize(v):
    if v and str(v) != "nan":
        str_v = str(v)
        if not str_v.isupper() and "http" not in str_v and len(str_v.split(" ")) < 2:
            return str_v.capitalize()
    return v


def capitalizeValues(df):
    columns = list(df.columns.values)
    for col in columns:
        df[col] = df[col].apply(checkIfCapitalize)
    return df
