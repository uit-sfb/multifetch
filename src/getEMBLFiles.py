from multiprocessing import Pool, cpu_count
from functools import partial
from utils import *
import datetime
import os
from pathlib import Path
import sys
import argparse
import shutil
import urllib3
from http.client import responses

def main(argv):
    parser = argparse.ArgumentParser(description='Set email and other parameters')
    parser.add_argument('-i', dest='inputList', default='', help='list of accessions')
    parser.add_argument('-l', dest='lost', default='', help='output for lost records list')
    parser.add_argument('-o', dest='out', default='', help='output directory')
    parser.add_argument('--one_proc', dest='one_proc', action='store_true', default=False, help='run in only one process')
    args = parser.parse_args()
    return args.inputList, args.lost, args.out, args.one_proc


def create_subdir(path):
    Path(path).mkdir(parents=True, exist_ok=True)
    return path


def move_files(source, destination):
    file_names = os.listdir(source)
    for file_name in file_names:
        if os.path.exists(source):
            shutil.move(os.path.join(source, file_name), os.path.join(destination, file_name))


def remove_dir(tmp):
    dirpath = Path(tmp)
    if dirpath.exists() and dirpath.is_dir():
        os.rmdir(dirpath)


def remove_folder(folder):
    dirpath = Path(folder)
    if dirpath.exists() and dirpath.is_dir():
        shutil.rmtree(dirpath, ignore_errors=True)


def clean_empty(path):
    try:
        os.remove(filePath)
    except Exception as e:
        print("Error while deleting file ", path)
        print(e)


def fetchNucleotideData(id, tmp):
    baseUrl = "https://www.ebi.ac.uk/ena/browser/api/embl"
    lineLimit = "lineLimit=1000"
    url = baseUrl + '/' + str(id) + '?' + lineLimit
    tmp_path = os.path.join(tmp, 'embl_{}.embl'.format(id))
    try:
        http = urllib3.PoolManager()
        with http.request('GET', url, preload_content=False) as req:
            http_status = req.status
            http_desc = responses[http_status]
            if not (http_desc == "OK"):
                return id
            else:
                with open(tmp_path, 'wb') as output:
                    shutil.copyfileobj(req, output)
    except Exception as e:
        print(e)
        return id


def pullFiles(input, proc_flag, tmp):
    records = getRecords(input)
    p_func = partial(fetchNucleotideData, tmp=tmp)
    if not proc_flag:
        print("Downloading embl files, CPUs used: {}".format(cpu_count()-1))
        pool = Pool(cpu_count()-1)
        results = pool.map(p_func, records)
        pool.close()
        pool.join()
    else:
        print("Downloading embl files in one process")
        results = list(map(p_func, records))
    return results


if __name__ == "__main__":
    input, lost, out, proc_flag = main(sys.argv[1:])
    print(datetime.datetime.now().time())
    print("Starting to download embl files")
    output = create_subdir(out)
    tmpdir = "/tmp/embl_tmp"
    remove_dir(tmpdir)
    tmp = create_subdir(tmpdir)
    lost_embl = pullFiles(input, proc_flag, tmp)
    move_files(tmp, output)
    remove_dir(tmpdir)
    getLostRecordList(lost_embl, lost)
