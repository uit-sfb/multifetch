from utils import *
import xml.etree.ElementTree as ET
from http.client import responses
import urllib3


def getBioProjectInfo(data):
    root = ET.fromstring(data)
    attrs = root.findall('./PROJECT/PROJECT_ATTRIBUTES/PROJECT_ATTRIBUTE/TAG')
    attr_vals = root.findall('./PROJECT/PROJECT_ATTRIBUTES/PROJECT_ATTRIBUTE/VALUE')
    title = [('bioproject:proj:name', t.text) for t in root.findall('./PROJECT/TITLE')]
    data = dict([elem for elem in zip(attrs, attr_vals)])
    tagsAndValues = list(map(lambda elem: ('bioproject:'+elem[0], elem[1]),[(t.text, v.text) for t, v in data.items()]))
    res = tagsAndValues + title
    return res


def processBioProjectData(accession, id):
    baseUrl = "https://www.ebi.ac.uk/ena/browser/api/xml"
    url = baseUrl + '/' + str(id)
    http = urllib3.PoolManager()
    request = http.request('GET', url)
    http_status = request.status
    http_desc = responses[http_status]
    df = pd.DataFrame()
    if not (http_desc == "OK"):
        pass
        # print("BioProject {} not fetched, reason: ".format(id) + str(http_status) + " " + str(http_desc))
    else:
        data = request.data.decode('UTF-8')
        df = pd.DataFrame(dict([('acc:genbank', accession)] + getBioProjectInfo(data)), index=[0])
    if not df.empty:
        df.columns = list(map(formatColumn, list(df.columns.values)))
    return df
