from Bio import SeqIO
import sys
import argparse
import os
import re
import shutil
from pathlib import Path
from multiprocessing import Pool, cpu_count
from functools import partial
from utils import getLostRecordList
from time import time as timer


def main(argv):
    parser = argparse.ArgumentParser(description='Set input, output and other parameters')
    parser.add_argument('-i', dest='input', default='', help='input folder with embl files')
    parser.add_argument('-o', dest='out', default='', help='output directory')
    parser.add_argument('--threshold', dest='threshold', default='', help='threshold for fasta file')
    parser.add_argument('--invalid_entries', dest='invalid_entries', default='', help='path to the list entries with an invalid genomic data')
    args = parser.parse_args()
    return args.input, args.out, args.threshold, args.invalid_entries


def parseEMBLFiles(filename):
    try:
        for record in SeqIO.parse(filename, "embl"):
            return record
    except Exception as err:
        print("Failed to parse {}, reason:\n {}\n".format(filename, err))


def removeTabs(s):
    return " ".join(s.split('\t'))


def validateFastaSize(filepath, threshold):
    f_size = os.path.getsize(filepath)
    if int(f_size) < int(threshold):
        raise Exception("{} file size is less than {}".format(filepath, threshold))


def writeSeqFasta(record, filePath, threshold):
    out_path = os.path.join(filePath,"assembly.fa")
    if os.path.exists(filePath):
        try:
            with open(out_path,"w") as out_seq:
                for line in record.format('fasta'):
                    out_seq.write(removeTabs(line))
            validateFastaSize(out_path, threshold)
        except Exception as e:
            print("Failed to download fasta due to {}\n".format(e))
            return record.id
    return None


def getFeatureList(recordId, feat):
    p = re.compile("\[(.*)\]")
    res = str(feat.location).split(', ')
    res_lst = [ "{}:".format(recordId) + "..".join(p.search(s).group(1).split(':')) for s in res]
    if len(res_lst) > 1:
        last_res = "join({})".format(",".join(res_lst))
    else:
        last_res = res_lst[0]
    return last_res


def writeFeaturesToFile(record, filePath, id):
    out_path = os.path.join(filePath,"protein.faa")
    if os.path.exists(filePath):
        gb_features = record.features
        feats = [feat for feat in gb_features if feat.type == "CDS"]
        counter = 0
        with open(out_path,"w") as out_handle:
            for feat in feats:
                counter += 1
                last_res = getFeatureList(id, feat)
                location = "[{}={}]".format("location", last_res)
                type = "[{}={}]".format("type", str(feat.type))
                qual_str = " ".join(["[{}={}]".format(k, v[0]) for k, v in feat.qualifiers.items() if (v[0] and k != "translation")])
                translation_str = feat.qualifiers["translation"][0]
                dataLine = " ".join([">{}_{}".format(id, counter), location, type, qual_str])
                out_handle.write(removeTabs(dataLine) + " " + "(id=SFB_COVID19_{})".format(id) + "\n" + removeTabs(translation_str) + "\n")


def getCDSData(organism, mainId, counter, location, product):
    if product:
        return " ".join([">{}_{}".format(mainId, counter), location,"[{}={}]".format("product", str(product[0])),"[{}={}]".format("organism", str(organism))])
    else:
        return " ".join([">{}_{}".format(mainId, counter), location,"[{}={}]".format("organism", str(organism))])


def writeCDSData(record, filePath, id):
    out_path = os.path.join(filePath,"cds.fna")
    if os.path.exists(filePath):
        feats = [feat for feat in record.features if feat.type == "CDS"]
        organism = record.annotations['organism']
        counter = 0
        with open(out_path, 'w') as out_f:
            for feat in feats:
                counter += 1
                last_res = getFeatureList(id, feat)
                location = "[{}={}]".format("location", last_res)
                product = feat.qualifiers.get("product","")
                dataLine = getCDSData(organism, id, counter, location, product)
                out_f.write(removeTabs(dataLine) + " " + "(id=SFB_COVID19_{})".format(id) + '\n' + removeTabs(str(feat.location.extract(record).seq)) + '\n')


def create_subdir(path):
    Path(path).mkdir(parents=True, exist_ok=True)
    return path


def move_files(source, destination):
    file_names = os.listdir(source)
    for file_name in file_names:
        if os.path.exists(source):
            shutil.move(os.path.join(source, file_name), os.path.join(destination, file_name))


def remove_folder(folder):
    dirpath = Path(folder)
    if dirpath.exists() and dirpath.is_dir():
        shutil.rmtree(dirpath, ignore_errors=True)


def remove_tmp(tmp):
    dirpath = Path(tmp)
    if dirpath.exists() and dirpath.is_dir():
        os.rmdir(dirpath)


def writeData(filename, tmp, output, threshold):
    fullName = os.path.join(input, filename)
    record = parseEMBLFiles(fullName)
    err_id = None
    if record:
        id = str(record.id)
        if id:
            if not os.path.exists(os.path.join(output, id)):
                tmp = create_subdir(os.path.join(tmp, id))
                err_id = writeSeqFasta(record, tmp, threshold)
                writeFeaturesToFile(record, tmp, id)
                writeCDSData(record, tmp, id)
    return err_id


def writeSeqOutput(input, output, threshold, entries_list):
    filenames = os.listdir(input)
    print("Downloading sequence data, CPUs used: {}".format(cpu_count()-1))
    root = "/tmp"
    remove_tmp(root + "/tmp")
    tmp = create_subdir(root + "/tmp")
    pool = Pool(cpu_count()-1)
    partial_func = partial(writeData, tmp=tmp, output=output, threshold=threshold)
    invalid_entries = pool.map(partial_func, filenames)
    pool.close()
    pool.join()
    move_files(tmp, output)
    invalid_entries = filter(lambda x: x != None, list(invalid_entries))
    remove_tmp(root + "/tmp")
    getLostRecordList(list(invalid_entries), entries_list)


if __name__ == "__main__":
   input, output, threshold, invalid_entries = main(sys.argv[1:])
   print("Starting to download sequence data")
   start = timer()
   writeSeqOutput(input, output, threshold, invalid_entries)
   print("Sequence download elapsed time: %s" % (timer() - start))
