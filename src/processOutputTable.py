from getBioSampleData import *
from getSRAData import *
from getEMBLMetadata import *
from getBioProjectData import *
from multiprocessing import Pool, cpu_count
from functools import partial
import datetime
import urllib
import re
import sys
import argparse
import os
import numpy as np


def main(argv):
    parser = argparse.ArgumentParser(description='Set email and other parameters')
    parser.add_argument('-i', dest='input', default='', help='input folder')
    parser.add_argument('-gen', dest='inputRelease', default='', help='input for release genome df')
    parser.add_argument('-t', dest='table', default='', help='path to accession table')
    parser.add_argument('-o', dest='out', default='', help='output directory')

    args = parser.parse_args()
    return args.input, args.inputRelease, args.table, args.out;


def mergeInOrder(attrs, proj, bio, embl, sra, release_gen):
    order_list = list(attrs.keys())
    acc_id = list(attrs.get('EMBL').values())[0]
    release_gen = release_gen.rename(columns={'release_genome:accession id':acc_id})
    dataframe_dict = {'BioProject':proj, 'BioSample':bio, 'EMBL':embl, 'SRA':sra, 'release_genome_meta.tsv':release_gen}
    df_list = [dataframe_dict.get(id, pd.DataFrame()) for id in order_list]
    df1, df2, df3, df4, df5, *_ = df_list
    df_merged_1 = mergeDataFrames(df1, df2)
    df_merged_2 = mergeDataFrames(df_merged_1, df3)
    df_merged_3 = mergeDataFrames(df_merged_2, df4)
    df_merged_4 = mergeDataFrames(df_merged_3, df5)
    df_merged_4 = renameColumns(df_merged_4)
    df_merged_4 = df_merged_4.groupby(level=0, axis=1).apply(lambda x: x.apply(sjoin, axis=1))
    df_merged_4 = removeDuplicatedData(df_merged_4)
    df_merged_4 = changeToNaN(df_merged_4)
    return df_merged_4


def mergeWithDefault(recordId, attrs, df, default):
    acc_id = list(attrs.get('EMBL').values())[0]
    default[acc_id] = recordId
    df = pd.merge(df, default, on=acc_id, how='left')
    df = renameColumns(df)
    df = df.groupby(level=0, axis=1).apply(lambda x: x.apply(sjoin, axis=1))
    df = removeDuplicatedData(df)
    return df


def getMetadataFromReleaseGenome(table):
    df = getTSVInput(table)
    df = lowercaseColumns(df)
    df = df.drop(['data source', 'quality assessment', 'submission date', 'virus strain name'], axis=1, errors='ignore')
    # Sometimes location is missing
    try:
        df['location'] = df['location'].apply(formatLocation)
    except KeyError as e:
        df['location'] = ""
    genDfCountries = list(df['location'].values)
    df = df.assign(isolation_country=pd.Series(getIsolationCountry(genDfCountries), dtype='object'))
    df = df.assign(geo_loc_name_GAZ=pd.Series(getGAZgeoLoc(genDfCountries), dtype='object'))
    source = ['release_genome:' + c for c in list(df.columns.values)]
    df.columns = source
    return df


def getMetadataFromSequencesTable(table):
    df = getTSVInput(table)
    df = lowercaseColumns(df)
    df = df.drop(['authors', 'genbank_title', 'release_date', 'sequence_type'], axis=1, errors='ignore')
    df['geo_location'] = df['geo_location'].apply(formatLocation)
    seqDfCountries = list(df['geo_location'].values)
    df = df.assign(isolation_country=pd.Series(getIsolationCountry(seqDfCountries), dtype='object'))
    df = df.assign(geo_loc_name_GAZ=pd.Series(getGAZgeoLoc(seqDfCountries), dtype='object'))
    return df


def getRowFromReleaseGenome(id, df):
    res = df.loc[df['release_genome:accession id'] == str(id).split('.')[0]]
    return res


def checkIfEmptyOrSpace(v):
    if v and v.strip():
        return v
    return np.nan


def changeToNaN(df):
    for c in list(df.columns):
        df[c] = df[c].apply(checkIfEmptyOrSpace)
    return df


def addDefaultValues(main_df, default_df):
    df = main_df.fillna(default_df)
    return df


def removeTabs(s):
    if s and s != np.nan and str(s) != "nan":
        return ' '.join(str(s).split('\t'))
    return s


def processParallel(recordName, input_path, default_columns, release_genome_df, attr_defs, default_df):
    fullName = os.path.join(input_path, recordName)
    record = parseEMBLFiles(fullName)
    if record:
        try:
            empty_df = pd.DataFrame(columns=default_columns)
            bioId = getBioSampleId(record)
            projId = getBioProjectId(record)
            release_row_df = getRowFromReleaseGenome(str(record.id), release_genome_df)
            release_row_df = updateAttributes(release_row_df, attr_defs.get('release_genome_meta.tsv'))
            release_row_df['release_genome:accession id'] = release_row_df['release_genome:accession id'].apply(getRecentVersion)
            proj_df = pd.DataFrame()
            bio_df = pd.DataFrame()
            sra_df = pd.DataFrame()
            embl_df = getMetadataFromEMBL(record, bioId)
            embl_df = updateAttributes(embl_df, attr_defs.get('EMBL'))
            if projId:
                proj_df = processBioProjectData(str(record.id), projId)
                proj_df = updateAttributes(proj_df, attr_defs.get('BioProject'))
            if bioId:
                bio_df = processBioSampleData(str(record.id), bioId)
                bio_df = updateAttributes(bio_df, attr_defs.get('BioSample'))

                sra_df = processSRAData(str(record.id), bioId)
                sra_df = updateAttributes(sra_df, attr_defs.get('SRA'))
            data = mergeInOrder(attr_defs, proj_df, bio_df, embl_df, sra_df, release_row_df)

            df = empty_df.append(data)
            df = df.applymap(removeTabs)
            df = addDefaultValues(df, default_df)
            return df
        except Exception as e:
            print("Failed to process record {}, Error:\n {}".format(record.id, e))
            return pd.DataFrame()


def processOutput(input, release, table, output):
    columns = getAllAttributes(table)
    default_values = zip(columns, getDefaultValues(table))
    default_df = pd.DataFrame(dict(default_values), index=[0])
    default_df.dropna(how='all', axis=1, inplace=True)

    empty_df = pd.DataFrame(columns=columns)
    release_df = getMetadataFromReleaseGenome(release)
    attributes = getAttributeOrder(table)
    inputList = os.listdir(input)
    print("Preparing to process the metadata, CPUs used: {}".format(cpu_count()))
    pool = Pool(cpu_count())
    process_func = partial(processParallel, input_path=input, default_columns=columns, release_genome_df=release_df, attr_defs=attributes, default_df=default_df)
    df_list = pool.map(process_func, inputList)
    pool.close()
    pool.join()
    print("Concatenate results into DataFrame")
    df = empty_df.append(df_list)
    df = addPrefix(df, columns)
    df.reset_index(drop=True, inplace=True)
    df = capitalizeValues(df)
    writeToTSV(df, output)


def getRecentVersion(id):
    if id is not None and id != "":
        baseUrl = "https://www.ebi.ac.uk/ena/browser/api/fasta"
        lineLimit = "lineLimit=1000"
        url = baseUrl + '/' + str(id) + '?' + lineLimit
        file = urllib.request.urlopen(url)
        header = file.readline().decode("utf-8")
        p = re.compile(">[A-Z]{3}(\|[\w]*(\.[\d])*)*")
        reg_exp = p.search(header).group(0)
        if reg_exp is not None:
            res = reg_exp.split('|')[-1:][0]
            return str(res)
        return id


def updateReleaseGenomeIds(df):
    df['release_genome:accession id'] = df['release_genome:accession id'].apply(getRecentVersion)
    return df


if __name__ == "__main__":
    input, release, table, output = main(sys.argv[1:])
    print(datetime.datetime.now().time())
    print("Starting to process metadata")
    processOutput(input, release, table, output)
