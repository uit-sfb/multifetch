import subprocess
from utils import *
import sys
import argparse


def main(argv):
    parser = argparse.ArgumentParser(description='Set email and other parameters')
    parser.add_argument('-i', dest='input', default='', help='input folder')
    parser.add_argument('-seq', dest='seqOut', default='', help='output directory for sequences')
    parser.add_argument('-gen', dest='inputRelease', default='', help='input for release genome df')
    parser.add_argument('-t', dest='table', default='', help='path to sfb table')
    parser.add_argument('-l', dest='lost', default='', help='output for lost records list')
    parser.add_argument('-o', dest='output', default='', help='output tsv')
    parser.add_argument('--lost_seq', dest='lost_seq', default='', help='output for list of missing genomic data')
    parser.add_argument('--threshold', dest='threshold', default='', help='threshold for fasta file')
    parser.add_argument('--invalid_entries', dest='invalid_entries', default='', help='path to the list entries with an invalid genomic data')
    parser.add_argument('--email', dest='email', default='', help='email for Entrez')
    parser.add_argument('--api_key', dest='api_key', default='', help='api key for Entrez')
    parser.add_argument('--skip_init', dest='skip_init', action='store_true', default=False, help='skip initial step')
    parser.add_argument('--skip_meta_download', dest='skip_meta_download', action='store_true', default=False, help='skip metadata download')
    parser.add_argument('--skip_seq_download', dest='skip_seq_download', action='store_true', default=False, help='skip sequence data download')
    parser.add_argument('--one_proc', dest='one_proc', action='store_true', default=False, help='run embl download in only one process')
    parser.add_argument('--mar_db', dest='mar_db', action='store_true', default=False, help='run multifetch for mar databases')

    args = parser.parse_args()
    commands = []
    
    if args.mar_db:
        downloadSeq = ['python3', 'downloadFtp.py', '-i', args.input, '-o', args.seqOut, '--lost_seq', args.lost_seq, '--email', args.email, '--api-key', args.api_key]
        downloadMeta = ['python3', 'getMarData.py', '-i', args.input, '-o', args.output, '-t', args.table, '-l', args.lost]
        if not args.skip_seq_download:
            commands.append(downloadSeq)
        if not args.skip_meta_download:
            commands.append(downloadMeta)
    else:
        if not args.skip_init:
            emblOut = "/tmp/embl"
            emblJob = ['python3', 'getEMBLFiles.py', '-i', args.input, '-l', args.lost, '-o', emblOut]
            if args.one_proc:
                emblJob.append('--one_proc')
            # No need to use Popen and communicate here.
            res = subprocess.run(emblJob, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            # If return code is != 0 (better using return code than stderr to find out the status!)
            if res.returncode:
                # Stdout contains both stdout and stderr
                raise Exception('Error occurred: %s' % res.stdout.decode("utf-8").strip('/n'))
        else:
            emblOut = args.input
        downloadSeq = ['python3', 'getSeqData.py', '-i', emblOut, '-o', args.seqOut, '--threshold', args.threshold, '--invalid_entries', args.invalid_entries]
        downloadMeta = ['python3', 'processOutputTable.py', '-i', emblOut, '-o', args.output, '-gen', args.inputRelease, '-t', args.table]
        if not args.skip_seq_download:
            commands.append(downloadSeq)
        if not args.skip_meta_download:
            commands.append(downloadMeta)

    # We start all the processes asynchronously
    procs = [subprocess.Popen(i, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) for i in commands]
    failed = False
    # For each process of the list...
    for p in procs:
        # If a previous process failed, we kill all the remaining ones
        if failed:
            p.terminate()
        else:
            # We wait for it to complete (if it has not completed yet)
            out, _ = p.communicate()
            # We set the failed flag to True
            failed = True
            # If return code is != 0 (better using return code than stderr to find out the status!)
            if p.returncode:
                # Stdout contains both stdout and stderr
                raise Exception('Error occurred: %s' % out.decode("utf-8").strip('/n'))


if __name__ == "__main__":
    main(sys.argv[1:])
