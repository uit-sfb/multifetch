from utils import *


def getCommentValue(lst):
    if len(lst) > 1:
        return lst[len(lst)-1].strip()
    elif lst:
        return lst[0].strip()
    else:
        return ''


def getMetadataFromEMBL(record, bioId):
    removeWhiteSpaces = lambda x: ' '.join(x.split())
    accession = str(record.id)
    gen_length = str(len(record.seq))
    proj_name = removeWhiteSpaces(removeColumn(record.description))
    dblink = record.dbxrefs
    biosample = bioId
    bioproject = getBioProjectId(dblink)
    anno = record.annotations['references']

    journals = ', '.join([removeColumn(ref.journal) for ref in anno if 'Unpublished' not in ref.journal])

    submission_date = getSubmissionDate(journals)
    submitter = getSubmittingLab(journals.split('.',1))
    publication_id_list = [ref.pubmed_id for ref in anno if ref.pubmed_id]
    publication_id = '|'.join(publication_id_list)

    embl_features = record.features
    isolate = getFeatureData('isolate', embl_features)
    isolation_source = getFeatureData('isolation_source', embl_features)
    host = getFeatureData('host', embl_features)
    collection_date = getFeatureData('collection_date', embl_features)
    collected_by = getFeatureData('collected_by', embl_features)

    countryFromFeatures = getFeatureData('country', embl_features)
    countryFromJournal = journals.split(', ')[-2:]

    locData = getCountryData(countryFromFeatures, countryFromJournal)
    loc_country, loc_name = getCounryAndName(locData)

    comm = record.annotations.get('comment', '')
    comm_list = []
    coverage_list = []
    seq_tech_list = []
    assembly_version_list = []
    assembly_name_list = []
    if comm:
        comm_list = comm.split('\n')
        coverage_list = [getCommentValue(s.split('::')) for s in comm_list if 'Coverage' in s]
        seq_tech_list = [getCommentValue(s.split('::')) for s in comm_list if 'Technology' in s]
        assembly_version_list = [getCommentValue(s.split('::')) for s in comm_list if 'Method' in s]
        assembly_name_list = [getCommentValue(s.split('::')) for s in comm_list if 'Name' in s]

    assembly_version = processSWData(getVersion(assembly_version_list))
    assebly_tool_name = processSWData(getToolName(assembly_version_list))
    assembly_name = splitNewLineAndReturnData(assembly_name_list)

    seq_depth = getCoverage(coverage_list)
    seq_method = splitNewLineAndReturnData(seq_tech_list)

    resList = [('acc:genbank', accession), ('embl:acc:bioproject', bioproject), ('embl:acc:biosample', biosample), ('embl:proj:name', proj_name), ('embl:proj:publication', publication_id), ('embl:host:scientific_name', host), ('embl:sampl:isolation_source', isolation_source),
    ('embl:sampl:collection_date', collection_date), ('embl:seq:submission_date', submission_date), ('embl:seq:submitter', submitter), ('embl:sampl:collected_by', collected_by), ('embl:sampl:isolate', isolate), ('embl:loc:country', loc_country), ('embl:loc:name', loc_name),
    ('embl:seq:method', seq_method), ('embl:seq:depth', seq_depth), ('embl:asmbl:sw_name', assebly_tool_name), ('embl:asmbl:sw_version', assembly_version), ('embl:assembly_name', assembly_name), ('embl:gen:length', gen_length)]
    
    df = pd.DataFrame(dict(resList), index=[0])
    if not df.empty:
        df.columns = list(map(formatColumn, list(df.columns.values)))
    return df
