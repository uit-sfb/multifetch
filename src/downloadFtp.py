from multiprocessing import Pool, cpu_count
from functools import partial
from Bio import Entrez
import sys
import argparse
import os
import pandas as pd
import xml.etree.ElementTree as ET
import urllib
import gzip
from io import BytesIO
from time import time as timer
from time import sleep
import shutil
from pathlib import Path
from utils import getLostRecordList
import subprocess


def flattenList(lst):
    return [item for sublist in lst for item in sublist]


def create_subdir(path):
    Path(path).mkdir(parents=True, exist_ok=True)
    return path


def getTSVInput(input):
    df = pd.read_csv(input, sep='\t', engine='python')
    df.columns = df.columns.str.strip()
    return df


def getRecords(input):
    df = getTSVInput(input)
    res = df['acc:assembly_genbank'].tolist()
    accList = [acc for acc in res if (acc and str(acc) != 'missing')]
    accList = list(set(accList))
    return accList


def main(argv):
    parser = argparse.ArgumentParser(description='Set email and other parameters')
    parser.add_argument('-i', dest='inputList', default='', help='list of accessions')
    parser.add_argument('-o', dest='out', default='', help='output directory')
    parser.add_argument('--lost_seq', dest='lost_seq', default='', help='output for list of missing genomic data')
    parser.add_argument('--email', dest='email', default='', help='entrez email')
    parser.add_argument('--api-key', dest='key', default='', help='entrez api-key')
    args = parser.parse_args()
    return args.inputList, args.out, args.lost_seq, args.email, args.key;


def get_path(path_obj):
    if path_obj is not None:
        return path_obj.text
    return None


def get_ftp_paths(url_path):
    postfix = url_path.split("/")[-1]
    ftp_path_fasta = os.path.join(url_path,"{}_genomic.fna.gz".format(postfix))
    ftp_path_nuc = os.path.join(url_path,"{}_cds_from_genomic.fna.gz".format(postfix))
    ftp_path_prot = os.path.join(url_path,"{}_protein.faa.gz".format(postfix))
    return ftp_path_fasta, ftp_path_nuc, ftp_path_prot


def get_url_paths(path_list):
    urls = flattenList(list(map(get_ftp_paths, path_list)))
    return urls


def generate_urls(id, out):
    file_path = os.path.join(out, id)
    if not os.path.exists(file_path) or (os.path.isdir(file_path) and not os.listdir(file_path)):
        try:
            handle_rec = Entrez.esearch(db="assembly", term=id)
            record = Entrez.read(handle_rec)
            handle_rec.close()
            sleep(0.1)  # sleep to prevent too many requests to Entrez
            for acc_id in record['IdList']:
                handle = Entrez.esummary(db="assembly", id=acc_id, report="full")
                tree = ET.fromstring(handle.read())
                handle.close()
                gca_path = get_path(tree.find('./DocumentSummarySet/DocumentSummary/FtpPath_GenBank'))
                path_list = list(filter(lambda x: x is not None,[gca_path]))
                urls = get_url_paths(path_list)
                return urls
        except Exception as e:
            print(e)
            return None


def rename_fasta(filename):
    if "cds_from_genomic.fna" in filename:
        return "cds.fna"
    if "genomic.fna" in filename:
        return "assembly.fa"
    if "protein.faa" in filename:
        return "protein.faa"
    print("{} is undefined".format(filename))
    return "undefined"


def get_id_name(url):
    elems = os.path.split(urllib.parse.urlsplit(url).path)[-1].split("_")[:2]
    id = "_".join(elems)
    return id


def download_and_unzip(url, tmp):
    response = urllib.request.urlopen(url)
    compressed = BytesIO(response.read())
    if compressed.getbuffer().nbytes > 0:
        file = gzip.GzipFile(fileobj=compressed)
        file_content = file.read()
        with open(tmp,"wb") as f:
            f.write(file_content)


def remove_tmp(tmp):
    dirpath = Path(tmp)
    if dirpath.exists() and dirpath.is_dir():
        bashCommand = "find {sf} -maxdepth 1 -type d -empty -delete".format(sf=dirpath)
        subprocess.run(bashCommand, shell=True, check=True, executable="/bin/bash")


def remove_folder(folder):
    dirpath = Path(folder)
    if dirpath.exists() and dirpath.is_dir():
        shutil.rmtree(dirpath)


def fetch_data(urls, tmp):
    lost_lst = []
    if urls:
        for url in urls:
            completed = False
            for x in range(10):
                try:
                    id = get_id_name(url)
                    tmp_subdir = create_subdir(os.path.join(tmp, id))
                    file_name = rename_fasta(os.path.split(urllib.parse.urlsplit(url).path)[-1].rstrip('.gz'))
                    tmp_path = os.path.join(tmp_subdir, file_name)
                    download_and_unzip(url, tmp_path)
                except urllib.error.URLError as e:
                    if not completed and x >= 9:
                        lost_lst.append(id + " " + url)
                        completed = True
                        break
                except Exception as e:
                    print("Failed to extract {}, error: \n{}, retrying.., attempt: {}".format(tmp_path, e, x+1))
                else:
                    completed = True
                    break
            if not completed:
                print("Script failed to download and extract {}".format(file_path))
    return lost_lst


def move_files(source, destination):
    file_names = os.listdir(source)
    for file_name in file_names:
        if os.path.exists(source):
            create_subdir(os.path.join(destination, file_name))
            sourceFolder = os.path.join(source, file_name)
            destFolder = os.path.join(destination, file_name)
            bashCommand = "find {sf} -maxdepth 1 -type f -exec mv -t {df} {{}} +".format(sf=sourceFolder, df=destFolder)
            subprocess.run(bashCommand, shell=True, check=True, executable="/bin/bash")


def pull_files(input, output, tmp):
    records = getRecords(input)
    print("Downloading marine sequence data, CPUs used: {}".format(cpu_count()-1))
    print("Create url list")
    create_urls = partial(generate_urls, out=output)
    urls = list(map(create_urls, records))
    lost_lst = []
    download = partial(fetch_data, tmp=tmp)
    pool = Pool(cpu_count()-1)
    print("Download marine sequence data from FTP server")
    lost_seq = pool.map(download, urls)
    lost_seq = flattenList(list(lost_seq))
    lost_seq = set(lost_seq)
    pool.close()
    pool.join()
    move_files(tmp, output)
    return lost_seq


if __name__ == "__main__":
    input, out, path_to_lost, email, key = main(sys.argv[1:])
    Entrez.email = email
    Entrez.api = key
    start = timer()
    root = "/tmp"
    # Remove tmp directory if exists
    remove_tmp(root + "/tmp")
    tmp = create_subdir(root + "/tmp")
    lost_seq = pull_files(input, out, tmp)
    remove_tmp(root + "/tmp")
    getLostRecordList(lost_seq, path_to_lost)
    print("Ftp sequence download elapsed time: %s" % (timer() - start))
