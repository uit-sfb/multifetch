# Multifetch

Multifetch is a set of tools for downloading and parsing biological data written in Python 3.6.
The tool is written to be configurable and fetch data from different sources.
The main source of the data currently is the [EBI Database.](https://www.ebi.ac.uk/) 

## Input data

Main input data for running the tool is stored in `data/contextual` folder.
For each database specific configuration will be added.
Example of the configuration table currently stored in `config` folder for testing purposes.

Multifetch tool uses the following input data:
- `accession_list.tsv`: list of accession ids for the entries to fetch
- `release_genome_meta.tsv`: table with additional info from the NCBI Virus database, serves as an additional datasource for the tool
- `multifetch.tsv`: configuration table which includes names of the sources as a column names with the first column describing ordered list of the attributes to be parsed. Empty columns are omitted. In front of the attribute names in the same row alternative name for the specific source is included to map the data from the source to the corresponding attribute. The output table is constructed based on the number of sources and attributes in the configuration table, as well as their order. 

The order in which source columns are placed describes the priority with the sources to the left being prioritized. If one attribute can be found in multiple sources, only the result from the most prioritized source will be left. `Default` column in the configuration table includes default values for the number of attributes. These values added at the end and only left if no data from other sources found for the attribute.  

## Output result

Output of the tool consists of:
- `out/metadata.tsv`: a tsv table with the collected data from the several sources. Columns represented by the attributes from the configuration table.
- `out/missing.tsv`: a list of genbank accession which were not found.
- `sequences`: a set of three fasta files (fasta sequence, nucleotides, proteins) for each entry, placed in the separate folder labeled with cdb_id. These files stored on s1 in the `/genomes` directory and used to make BLAST database.
